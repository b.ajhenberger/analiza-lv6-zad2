﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke,
//i u svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu funkcionalnost koju biste očekivali od takve igr
//e.Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.
namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Random rng = new Random();
        public int broj_zivota=3;
        List<string> pojmovi = new List<string>();
        string path = "C:\\pojmovi.txt";
        string line;
        int n, i, ii;
        string pojam;
        char[] ispis;

        private void Ucitaj()
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
                while ((line = reader.ReadLine()) != null)
                {
                    pojmovi.Add(line);
                }
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e) {
            Ucitaj();
            label3.Text = "";
            n= pojmovi.Count();
            i = rng.Next(0, n);
            pojam = pojmovi[i];
            ii = pojam.Length;
            ispis = new char[ii];
            broj_zivota = 3;
            for(int a = 0; a < ii; a++)
            {
                ispis[a] = '?';
                label3.Text = label3.Text + " ? ";
            }
            broj_zivota = 3;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool pogodak=false;
            label3.Text = "";
            char slovo = textBox1.Text.Last();
            for(int a = 0; a < ii; a++)
            {
             if(slovo == pojam.ElementAt(a))
                {
                    pogodak = true;
                    ispis[a]= slovo;
                }
 
            }
            if (pogodak == false)
            {
                broj_zivota = broj_zivota - 1;
            }
            for(int a = 0; a < ii; a++)
            {
                label3.Text = label3.Text + ispis[a];
            }
            label2.Text = "Broj Zivota = " + broj_zivota.ToString();
            if (broj_zivota == 0)
            {
                label3.Text = "Kraj igre";
            }

        }
    }
}
